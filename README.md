# NoiseCrypto

Encrypt files and communication using entropy one time pads that can be tanked so the shared entropy secret one time pad channel is sustained once established. [![Liberapay](donate.svg "Liberapay")](https://liberapay.com/mahakal/donate)  